import { migrate } from 'postgres-migrations'
import { createUnpooledPostgresConnection, withUnpooledConnection } from '@shared/db';
import path from 'path'
import logger from '@shared/Logger';
import { exec, isGitpod } from '../util/gitpod'
import { exists, readFile, rmrf } from '../util/file'

async function startPostgres() {
    logger.info(`starting postgres`)
    try {
        const result = await exec(`pg_ctl status`)
        logger.info(`pgctl_status returned: ${result}`)
        if (result.indexOf(`pg_ctl: no server running`) !== -1) {
            await exec(`pg_start`)
        } else {
            logger.info(`postgres was already started`)
        }         
    } catch (e) {
        logger.info(`suppressing error: ${JSON.stringify(e)}`)
    }
    
}

async function stopPostgres() {
    logger.info(`stopping postgres`)
    try {
        await exec(`pg_stop`)
    } catch (e) {
        logger.info(`suppressing error: ${JSON.stringify(e)}`)
    }
}

async function takeSnapshot() {
    await stopPostgres()

    logger.info(`deleting existing snapshot`)
    await rmrf(`/workspace/.pgsnap`)
    logger.info(`taking new snapshot`)
    await exec(`cp -R /workspace/.pgsql /workspace/.pgsnap`)
    
    await startPostgres()
}

async function applySnapshot() {
    await stopPostgres()

    logger.info(`deleting existing database`)
    await rmrf(`/workspace/.pgsql`)
    logger.info(`applying snapshot`)
    await exec(`cp -R /workspace/.pgsnap /workspace/.pgsql`)

    await startPostgres()

}

async function applyCurrentMigration() {
    logger.info(`executing "current" migration`)
    await exec(`psql -h localhost -d postgres -f src/current.sql`)
}

export async function initializeDatabase() {
    const migrationsDir = path.join(__dirname, `..`,`migrations`)
    
    const runMigrations = async () => {
        await startPostgres()
        await withUnpooledConnection(async client => {
            await migrate({client}, migrationsDir)
            logger.info(`migrations complete`)
        })
    }

    logger.info(`Using migrations directory: ${migrationsDir}`)
    if (isGitpod()) {
        logger.info(`running migrations in gitpod mode`)
        const snapshotExists = await exists(`/workspace/.pgsnap`)
        if (snapshotExists) {
            logger.info(`using existing snapshot`)
            // use snapshot
            await applySnapshot()
        }  else {
            logger.info(`running migration prior to taking snapshot`)

            await runMigrations()

            logger.info(`taking initial snapshot`)
            await takeSnapshot()
        }
        await applyCurrentMigration()
    } else {
        logger.info(`running migrations in normal deployment mode`)
        await runMigrations()
    }
}
