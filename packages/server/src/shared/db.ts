import pg from 'pg'
import logger from '@shared/Logger';

function getGitpodPostgresOptions() {
    return {
        user: `gitpod`,
        host: `127.0.0.1`,
        database: `postgres`,
        port: 5432,
    }
}

// function createGitpodPool() {
//     logger.info(`using Gitpod pool`)
//     return new pg.Pool(getGitpodPostgresOptions())
// }



// function createEnvPool() {
//     logger.info(`using environment pool`)
//     return new pg.Pool()
// }

// function makePool() {
//     return process.env.PGHOST ? createEnvPool() : createGitpodPool()
// }
// const pool = [makePool()]


// export function stopPool() {
//     logger.info(`stopping postgres pool`)
//     return new Promise(function (resolve, reject) {
//         pool[0].end(() => {
//             logger.info(`pool stopped`)
//             resolve(true)
//         })
//     })
// }

// export function startPool() {
//     logger.info(`starting postgres pool`)
//     pool[0] = makePool()
//     logger.info(`pool started`)
// }



// export async function withDb(callback: (db: pg.PoolClient) => Promise<any>) {
//     const client = await pool[0].connect()
//     await callback(client)
// }

export async function createUnpooledPostgresConnection() {
    const result = process.env.PGHOST ? new pg.Client() : new pg.Client(getGitpodPostgresOptions())
    await result.connect()
    return result
}

export async function withUnpooledConnection(callback: (db: pg.Client) => Promise<any>) {
    const client = await createUnpooledPostgresConnection()
    try {
        await callback(client)
    } finally {
        await client.end()
    }
}



// export function getDatabaseUrl() {
//     return process.env.PGHOST ?
//         `postgres://${process.env.PGUSER}:${process.env.PGPASSWORD}@${process.env.PGHOST}:${process.env.PGPORT}/${process.env.PGDATABASE}`
//         : `postgres://gitpod@127.0.0.1:5432/postgres`;
// }
