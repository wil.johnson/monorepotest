create user cyton;
create schema cyton authorization cyton;
create schema cyton_hidden authorization cyton;

create user cyton_user;
grant usage on schema cyton, cyton_hidden to cyton_user;

set local role cyton;

create table cyton_hidden.users (
    id                  integer generated by default as identity primary key,
    external_id         text unique,
    preferred_username  text,
    email               text,
    given_name          text,
    family_name         text,
    roles               jsonb
);

grant select, insert, update, delete on cyton_hidden.users to cyton_user;


create or replace view cyton.user_session as 
with token as (
    select cast(current_setting('cyton.user_session.jwt', 't') as json) as jwt
),
session as (
    select 
        t.jwt ->> 'sub' as external_id,
        t.jwt
    from token t
)
select 
    u.id,
    s.external_id,
    s.jwt,
    s.jwt -> 'realm_access' -> 'roles' as roles,
    s.jwt ->> 'preferred_username'     as preferred_username,
    s.jwt ->> 'given_name'             as given_name,
    s.jwt ->> 'family_name'            as family_name,
    s.jwt ->> 'email'                  as email        
from session s
left outer join cyton_hidden.users u
    on u.external_id = s.external_id;

grant select on cyton.user_session to cyton_user;

create or replace function cyton.current_user_id() returns integer as $$
declare
    resultId integer;
begin
    select 
        id into resultId
    from cyton.user_session;
    return resultId;
end;
$$ language plpgsql;

create or replace function cyton.is_logged_in() returns boolean as $$
begin
    return (select external_id from cyton.user_session) is not null;
end;
$$ language plpgsql;

create or replace function cyton.is_current_user(id integer) returns boolean as $$
begin
    return id is not null and id = cyton.current_user_id();
end;
$$ language plpgsql;

create or replace function cyton.current_user_has_role(role text) returns boolean as $$
begin
    return exists (
        select 1 from cyton_hidden.users 
        where id = cyton.current_user_id()
            and roles? role
    );
end;
$$ language plpgsql;

create policy user_select_policy on cyton_hidden.users
for select
using (is_logged_in());

create policy user_insert_policy on cyton_hidden.users
for insert
with check (is_logged_in());

create policy user_update_policy on cyton_hidden.users
for update
using (is_current_user(id));

create policy user_delete_policy on cyton_hidden.users
for delete
using (is_current_user(id));

alter table cyton_hidden.users enable row level security;

       
create or replace function cyton.register_user() returns setof cyton.user_session as $$
declare
    externalId text;
begin
    select 
        external_id into externalId
    from cyton.user_session;

    if externalId is null then
        raise exception 'cyton.user_session.external_id setting is null'
            using hint = 'cyton.user_session is populated from the JWT token';
    end if;

    if exists(
        select 1 from cyton_hidden.users
        where external_id = externalId
    ) then
        update cyton_hidden.users 
        set (external_id, preferred_username, email, given_name, family_name, roles) = (
            select external_id, preferred_username, email, given_name, family_name, roles
            from cyton.user_session
        )
        where external_id = externalId;
    
    else
        insert into cyton_hidden.users (external_id, preferred_username, email, given_name, family_name, roles)
                    select external_id, preferred_username, email, given_name, family_name, roles
                    from cyton.user_session;
    end if;

    return query select * from cyton.user_session;
end;
$$ LANGUAGE plpgsql;

commit;