import './pre-start'; // Must be the first import
import app from '@server';
import logger from '@shared/Logger';
import { initializeDatabase } from './initializers/initDatabase'



async function start() {
    await initializeDatabase()
    
    // Start the server
    const port = Number(process.env.PORT || 4000)
    app.listen(port, () => {
        logger.info('Express server started on port: ' + port)
    })
}

start()
