import express = require('express')
import cors = require('cors')
import PgPubsub from '@graphile/pg-pubsub'
import * as jwt from 'express-jwt'
import * as jwksRsa from 'jwks-rsa'
import * as jwtDecode from 'jwt-decode'
import {
    GraphQLErrorExtended,
    makePluginHook,
    postgraphile,
} from 'postgraphile'
import * as PostgraphileLogConsola from 'postgraphile-log-consola'
import { extendedFormatError } from 'postgraphile/build/postgraphile/extendedFormatError'
import logger from '@shared/Logger';

import bodyParser = require('body-parser')
import { IncomingMessage, ServerResponse } from 'http'


export type ApiOptions = {
    role: string
    schemas: string[]
    corsAllowed: string[]
    disableDefaultMutations?: boolean
    plugins?: any[]
}

/* TODO FIXME comebackhere
export async function initApi(
    apiOptions: ApiOptions,
    app: any 
) {
    const {
        corsAllowed,
        disableDefaultMutations,
        role,
        schemas,
    } = apiOptions

    const {
        baseDomain,
        auth: { baseAuthUrl, realm },
        letsEncryptStaging,
        ...rest
    } = serverOptions

    const strictSsl = !letsEncryptStaging

    const checkJwt = jwt({
        secret: jwksRsa.expressJwtSecret({
            cache: true,
            rateLimit: true,
            jwksRequestsPerMinute: 5,
            jwksUri: `${baseAuthUrl}/realms/${realm}/protocol/openid-connect/certs`,
            strictSsl
        }),
        audience: `account`,
        issuer: `${baseAuthUrl}/realms/${realm}`,
        algorithms: ['RS256'],
        credentialsRequired: false,
    })

    log.info(
        `initializing CORS options with allowed origins: ${JSON.stringify(
            corsAllowed
        )}`
    )
    const corsOptions = {
        origin: function (origin, callback) {
            log.info(`Checking origin: ${origin}`)
            if (origin && corsAllowed.indexOf(origin) == -1) {
                log.info(`Rejecting origin: ${origin}`)
                callback(new Error(`Not allowed by CORS: ${origin}`))
            } else {
                callback(null, true)
            }
        },
        credentials: true,

        allowedHeaders: [
            'Origin',
            'X-Requested-With',
            // Used by `express-graphql` to determine whether to expose the GraphiQL
            // interface (`text/html`) or not.
            'Accept',
            // Used by PostGraphile for auth purposes.
            'Authorization',
            // Used by GraphQL Playground and other Apollo-enabled servers
            'X-Apollo-Tracing',
            // The `Content-*` headers are used when making requests with a body,
            // like in a POST request.
            'Content-Type',
            'Content-Length',
            // For our 'Explain' feature
            'X-PostGraphile-Explain',
        ],
    }
    const withCors = cors(corsOptions)

    const getPgSettings = async (request: any) => {
        const settings = {}

        settings[`role`] = role
        settings[`search_path`] = schemas.join(`,`)
        settings[`cyton.user_session.jwt`] = JSON.stringify(request.user || {})

        log.info(`Using PgSettings:`, settings)
        return settings
    }

    const additionalGraphQLContextFromRequest = async (
        request: IncomingMessage,
        response: ServerResponse
    ) => {
        const jwtClaims = (request as any).user

        const cyton: CytonContext = {
            user: {
                jwtClaims,
            },
            userHasRole: (role: string) =>  {
                const claims = cyton.user?.jwtClaims
                if (claims) {
                    return claims.realm_access.roles.indexOf(role) !== -1
                }
                return false
            }
        }

        return { cyton }
    }

    const PgSimplifyInflectorPlugin = require('@graphile-contrib/pg-simplify-inflector')
    const SubscriptionPlugin = require('@graphile/subscriptions-lds').default

    const pluginHook = makePluginHook([PostgraphileLogConsola, PgPubsub])

    const extendedErrors = [
        'severity',
        'code',
        'detail',
        'hint',
        'position',
        'internalPosition',
        'internalQuery',
        'where',
        'schema',
        'table',
        'column',
        'dataType',
        'constraint',
        'file',
        'line',
        'routine',
    ]

    const formatError = (error) => {
        const formattedError = extendedFormatError(error, extendedErrors)
        formattedError['stack'] = error.stack
        log.error(formattedError)
        return formattedError as GraphQLErrorExtended
    }

    let plugins = [PgSimplifyInflectorPlugin, SubscriptionPlugin]
    if (apiOptions.plugins) {
        plugins = [...plugins, ...apiOptions.plugins]
    }

    // Apply CORS to the graphql endpoint
    app.options('/graphql', withCors)
    app.use(
        withCors,
        checkJwt,
        postgraphile(postgresUrl, schemas, {
            additionalGraphQLContextFromRequest,
            disableDefaultMutations,
            pluginHook,
            subscriptions: true, // start the websocket server
            simpleSubscriptions: true, // Add the `listen` subscription field
            dynamicJson: true,
            live: true,
            watchPg: true,
            graphiql: true,
            enhanceGraphiql: true,
            ignoreRBAC: false,
            pgSettings: getPgSettings,
            handleErrors: (errors) => {
                return errors.map(formatError)
            },
            ownerConnectionString: postgresUrl,
            appendPlugins: plugins,
            graphileBuildOptions: {
            },
        })
    )
}
*/