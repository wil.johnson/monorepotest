import logger from '@shared/Logger';
import { exists, readFile, rmrf } from './file'
import * as child from 'child_process';

export function exec(command: string) {
    return new Promise<string>(function (resolve, reject) {
        child.exec(command, (error, stdout, stderr) => {
            logger.info(`executed command "${command}": ${stdout}, ${stderr}`)
            resolve(stdout)
        })
    })
}

export function isGitpod() {
    const id = process.env.GITPOD_WORKSPACE_ID || ``
    return id.length > 0
}
