import * as fs from 'fs'
import * as path from 'path'
import * as mkdirp from 'mkdirp'
import * as util from 'util'
import logger from '@shared/Logger';
import rimraf from 'rimraf'
import { sleep } from './asyncUtil'

export const asyncWriteFile = util.promisify(fs.writeFile)

export const asyncUnlink = util.promisify(fs.unlink)

export async function waitForResult<T>(
    producer: () => Promise<T>,
    filter: (content: T) => boolean,
    interval: number = 1000
) {
    let result = false
    while (result === false) {
        const content = await producer()
        logger.info(`waitForResult producer returned content`)
        result = filter(content)
        if (result) {
            logger.info(`content matched filter`)
            return content
        } else {
            logger.info(`content did not match filter, retrying...`)
            await sleep(interval)
        }
        
    }
}

export function waitForFile(filePath: string) {
    return new Promise(function (resolve, reject) {
        const tryRead = () => {
            logger.info(`attempting to read file ${filePath}`)
            fs.readFile(filePath, (err, data) => {
                if (err) {
                    logger.info(`error reading file ${filePath}`)
                    setTimeout(tryRead, 5000)
                } else {
                    logger.info(`Loaded data from file ${filePath}`)
                    resolve(data.toString())
                }
            })
        }

        tryRead()
    })
}


export async function readOrCreate(
    filePath: string,
    loadInitialData: () => Promise<string>
) {
    return new Promise(function (resolve, reject) {
        const onFile = (arg1?: any, arg2?: any) => {
            fs.readFile(filePath, (err, data) => {
                if (err) {
                    logger.err(`error reading file ${filePath}: ${err}`)
                    reject(err)
                } else {
                    const result = data.toString()
                    resolve(result)
                }
            })
        }

        fs.access(filePath, fs.constants.R_OK, function (err) {
            if (err) {
                loadInitialData().then((data) => {
                    fs.writeFile(filePath, data, onFile)
                })
            } else {
                onFile()
            }
        })
    }) as Promise<string>
}

export async function readFile(filePath: string) {
    return new Promise(function (resolve, reject) {
        const onFile = (arg1?: any, arg2?: any) => {
            fs.readFile(filePath, (err, data) => {
                if (err) {
                    logger.err(`error reading file ${filePath}: ${err}`)
                    reject(err)
                } else {
                    const result = data.toString()
                    resolve(result)
                }
            })
        }

        fs.access(filePath, fs.constants.R_OK, function (err) {
            if (err) {
                reject(err)
            } else {
                onFile()
            }
        })
    }) as Promise<string>
}

export function rmrf(path: string) {
    return new Promise(function (resolve, reject) {
        rimraf(path, (e) => {
            if (e) {
                reject(e)
            } else {
                resolve(true)
            }
        })
    })
}

export function exists(path: string) {
    logger.info(`checking if path "${path}" exists`)
    return new Promise(function (resolve, reject) {
        fs.access(path, err => {
            if (err) {
                logger.info(`exists: err: ${err}`)
                resolve(false)
            } else {
                logger.info(`path does exist: "${path}"`)
                resolve(true)
            }
        })
    })
}


