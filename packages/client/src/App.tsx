import React from 'react';
import useSWR from 'swr'
import logo from './logo.svg';
import './App.css';

const fetcher = (url: string) => fetch(url).then((res) => res.json())

function App() {
//   const { data: result, error } = useSWR(`/api/healthcheck`, fetcher)    
  const dbg = useSWR(`/api/healthcheck`, fetcher)    
  return (
    <div className="App">
        <h1>{JSON.stringify(dbg)}</h1>
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.tsx</code> and save to reload.
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
      </header>
    </div>
  );
}

export default App;
